package Zavrsni_rad.web_aplikacija.domain;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.Table;

@Entity
@Table(name="rezultat_student")
@IdClass(RezultatStudentKey.class)
public class RezultatStudent {
    @Id
    private int idObavljenTest;
    @Id
    private int idPitanjePovijest;
    @Id
    private int idStudent;
    private boolean tocnostOdg;

    public RezultatStudent() {}

    public RezultatStudent(int idObavljenTest, int idPitanjePovijest, int idStudent, boolean tocnostOdg) {
        this.idObavljenTest = idObavljenTest;
        this.idPitanjePovijest = idPitanjePovijest;
        this.idStudent = idStudent;
        this.tocnostOdg = tocnostOdg;
    }

    public int getIdObavljenTest() {
        return idObavljenTest;
    }

    public void setIdObavljenTest(int idObavljenTest) {
        this.idObavljenTest = idObavljenTest;
    }

    public int getIdPitanjePovijest() {
        return idPitanjePovijest;
    }

    public void setIdPitanjePovijest(int idPitanjePovijest) {
        this.idPitanjePovijest = idPitanjePovijest;
    }

    public int getIdStudent() {
        return idStudent;
    }

    public void setIdStudent(int idStudent) {
        this.idStudent = idStudent;
    }

    public boolean isTocnostOdg() {
        return tocnostOdg;
    }

    public void setTocnostOdg(boolean tocnostOdg) {
        this.tocnostOdg = tocnostOdg;
    }
}
