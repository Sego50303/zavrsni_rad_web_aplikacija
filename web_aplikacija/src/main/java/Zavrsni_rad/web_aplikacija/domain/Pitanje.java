package Zavrsni_rad.web_aplikacija.domain;

import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;

import javax.persistence.*;
import java.util.List;

@Entity
@Table(name="pitanje")
public class Pitanje {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int idPitanje;
    private char idVrstaPitanje;
    private String tekstPitanje;
    private float bodToc;
    private float bodNeToc;
    private float bodNeOdg;
    private String temaPitanje;

    @ManyToOne
    @JoinColumn(name = "idVrstaPitanje", insertable = false, updatable = false,
            referencedColumnName = "idVrstaPitanje", nullable = false)
    @Fetch(FetchMode.JOIN)
    private VrstaPitanje vrstaPitanje;

    @OneToMany(mappedBy = "pitanje")
    @Fetch(FetchMode.JOIN)
    private List<PonudeniOdgovor> ponudeniOdgovori;

    public Pitanje() {}

    public Pitanje(int idPitanje, char idVrstaPitanje, String tekstPitanje, float bodToc, float bodNeToc, float bodNeOdg, String TemaPitanje) {
        this.idPitanje = idPitanje;
        this.idVrstaPitanje = idVrstaPitanje;
        this.tekstPitanje = tekstPitanje;
        this.bodToc = bodToc;
        this.bodNeToc = bodNeToc;
        this.bodNeOdg = bodNeOdg;
        this.temaPitanje = temaPitanje;
    }

    public int getIdPitanje() {
        return idPitanje;
    }

    public void setIdPitanje(int idPitanje) {
        this.idPitanje = idPitanje;
    }

    public char getIdVrstaPitanje() {
        return idVrstaPitanje;
    }

    public void setIdVrstaPitanje(char idVrstaPitanje) {
        this.idVrstaPitanje = idVrstaPitanje;
    }

    public String getTekstPitanje() {
        return tekstPitanje;
    }

    public void setTekstPitanje(String tekstPitanje) {
        this.tekstPitanje = tekstPitanje;
    }

    public float getBodToc() {
        return bodToc;
    }

    public void setBodToc(float bodToc) {
        this.bodToc = bodToc;
    }

    public float getBodNeToc() {
        return bodNeToc;
    }

    public void setBodNeToc(float bodNeToc) {
        this.bodNeToc = bodNeToc;
    }

    public float getBodNeOdg() {
        return bodNeOdg;
    }

    public void setBodNeOdg(float bodNeOdg) {
        this.bodNeOdg = bodNeOdg;
    }

    public String getTemaPitanje() {
        return temaPitanje;
    }

    public void setTemaPitanje(String temaPitanje) {
        this.temaPitanje = temaPitanje;
    }

    public VrstaPitanje getVrstaPitanje() {
        return vrstaPitanje;
    }

    public void setVrstaPitanje(VrstaPitanje vrstaPitanje) {
        this.vrstaPitanje = vrstaPitanje;
    }

    public List<PonudeniOdgovor> getPonudeniOdgovori() {
        return ponudeniOdgovori;
    }

    public void setPonudeniOdgovori(List<PonudeniOdgovor> ponudeniOdgovori) {
        this.ponudeniOdgovori = ponudeniOdgovori;
    }
}
