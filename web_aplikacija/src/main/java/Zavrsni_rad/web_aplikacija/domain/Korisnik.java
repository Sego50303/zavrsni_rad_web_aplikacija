package Zavrsni_rad.web_aplikacija.domain;

import javax.persistence.*;

@Entity
@Table(name="korisnik")
public class Korisnik {
    @Id
    @Column(name="idKorisnik")
    private int idKorisnik;

    @Column(name="imeKorisnik")
    private String imeKorisnik;
    @Column(name="lozinkaKorisnik")
    private String lozinkaKorisnik;

    public Korisnik(){}

    public Korisnik(int idKorisnik, String  imeKorisnik, String lozinkaKorisnik){
        this.idKorisnik=idKorisnik;
        this.imeKorisnik=imeKorisnik;
        this.lozinkaKorisnik=lozinkaKorisnik;
    }

    public int getIdKorisnik() {
        return idKorisnik;
    }

    public void setIdKorisnik(int idKorisnik) {
        this.idKorisnik = idKorisnik;
    }

    public String getImeKorisnik() {
        return imeKorisnik;
    }

    public void setImeKorisnik(String imeKorisnik) {
        this.imeKorisnik = imeKorisnik;
    }

    public String getLozinkaKorisnik() {
        return lozinkaKorisnik;
    }

    public void setLozinkaKorisnik(String lozinkaKorisnik) {
        this.lozinkaKorisnik = lozinkaKorisnik;
    }
}
