package Zavrsni_rad.web_aplikacija.domain;

import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;

import javax.persistence.*;

@Entity
@Table(name="odgovor_povijest")
public class OdgovorPovijest {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int idOdgovorPovijest;
    private int idPitanjePovijest;
    private String tekstOdg;
    private boolean tocanOdg;

    @ManyToOne
    @JoinColumn(name = "idPitanjePovijest", insertable = false, updatable = false,
            referencedColumnName = "idPitanjePovijest", nullable = false)
    @Fetch(FetchMode.JOIN)
    private PitanjePovijest pitanjePovijest;

    public OdgovorPovijest() {}

    public OdgovorPovijest(int idPitanjePovijest, String tekstOdg, boolean tocanOdg) {
        this.idPitanjePovijest = idPitanjePovijest;
        this.tekstOdg = tekstOdg;
        this.tocanOdg = tocanOdg;
    }

    public OdgovorPovijest(int idOdgovorPovijest, int idPitanjePovijest, String tekstOdg, boolean tocanOdg) {
        this.idOdgovorPovijest = idOdgovorPovijest;
        this.idPitanjePovijest = idPitanjePovijest;
        this.tekstOdg = tekstOdg;
        this.tocanOdg = tocanOdg;
    }

    public int getIdOdgovorPovijest() {
        return idOdgovorPovijest;
    }

    public void setIdOdgovorPovijest(int idOdgovorPovijest) {
        this.idOdgovorPovijest = idOdgovorPovijest;
    }

    public int getIdPitanjePovijest() {
        return idPitanjePovijest;
    }

    public void setIdPitanjePovijest(int idPitanjePovijest) {
        this.idPitanjePovijest = idPitanjePovijest;
    }

    public String getTekstOdg() {
        return tekstOdg;
    }

    public void setTekstOdg(String tekstOdg) {
        this.tekstOdg = tekstOdg;
    }

    public boolean isTocanOdg() {
        return tocanOdg;
    }

    public void setTocanOdg(boolean tocanOdg) {
        this.tocanOdg = tocanOdg;
    }

}
