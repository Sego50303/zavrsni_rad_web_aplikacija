package Zavrsni_rad.web_aplikacija.domain;

import java.util.Map;

public class TestInfo {
    private int brStud;
    private int maxBod;
    private float prosjek;
    private Map<Integer, Float> pitanjaProsjek;  // idPitanje - postotak tocnih

    public TestInfo() {}

    public TestInfo(int brStud, int maxBod, int prosjek, Map<Integer, Float> pitanjaProsjek) {
        this.brStud = brStud;
        this.maxBod = maxBod;
        this.prosjek = prosjek;
        this.pitanjaProsjek = pitanjaProsjek;
    }

    public int getBrStud() {
        return brStud;
    }

    public void setBrStud(int brStud) {
        this.brStud = brStud;
    }

    public int getMaxBod() {
        return maxBod;
    }

    public void setMaxBod(int maxBod) {
        this.maxBod = maxBod;
    }

    public float getProsjek() {
        return prosjek;
    }

    public void setProsjek(float prosjek) {
        this.prosjek = prosjek;
    }

    public Map<Integer, Float> getPitanjaProsjek() {
        return pitanjaProsjek;
    }

    public void setPitanjaProsjek(Map<Integer, Float> pitanjaProsjek) {
        this.pitanjaProsjek = pitanjaProsjek;
    }
}
