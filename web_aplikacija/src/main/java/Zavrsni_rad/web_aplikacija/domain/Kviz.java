package Zavrsni_rad.web_aplikacija.domain;

import javax.persistence.*;

@Entity
@Table(name="kviz")
public class Kviz {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int idKviz;
    //@Column(name = "idNastCjel")
    private int idNastCjel;
    private String imeKviz;
    private String kratkiOpisKviz;
    private String opisKviz;
    private int redniBrojKviz;
    @Column(nullable = true)
    private Integer idKvizPovijest;

    @ManyToOne
    @JoinColumn(name = "idNastCjel", insertable = false, updatable = false,
            referencedColumnName = "idNastavnaCjelina", nullable = false)
    private NastavnaCjelina nastavnaCjelina;

    public Kviz() {}

    public Kviz(int idNastCjel, String imeKviz, String kratkiOpisKviz, String opisKviz, int redniBrojKviz) {
        this.idNastCjel = idNastCjel;
        this.imeKviz = imeKviz;
        this.kratkiOpisKviz = kratkiOpisKviz;
        this.opisKviz = opisKviz;
        this.redniBrojKviz = redniBrojKviz;
    }

    public Kviz(int idKviz, int idNastCjel, String imeKviz, String kratkiOpisKviz, String opisKviz, int redniBrojKviz) {
        this.idKviz = idKviz;
        this.idNastCjel = idNastCjel;
        this.imeKviz = imeKviz;
        this.kratkiOpisKviz = kratkiOpisKviz;
        this.opisKviz = opisKviz;
        this.redniBrojKviz = redniBrojKviz;
    }

    public int getIdKviz() {
        return idKviz;
    }

    public void setIdKviz(int idKviz) {
        this.idKviz = idKviz;
    }

    public int getIdNastCjel() {
        return idNastCjel;
    }

    public void setIdNastCjel(int idNastCjel) {
        this.idNastCjel = idNastCjel;
    }

    public String getImeKviz() {
        return imeKviz;
    }

    public void setImeKviz(String imeKviz) {
        this.imeKviz = imeKviz;
    }

    public String getKratkiOpisKviz() {
        return kratkiOpisKviz;
    }

    public void setKratkiOpisKviz(String kratkiOpisKviz) {
        this.kratkiOpisKviz = kratkiOpisKviz;
    }

    public String getOpisKviz() {
        return opisKviz;
    }

    public void setOpisKviz(String opisKviz) {
        this.opisKviz = opisKviz;
    }

    public int getRedniBrojKviz() {
        return redniBrojKviz;
    }

    public void setRedniBrojKviz(int redniBrojKviz) {
        this.redniBrojKviz = redniBrojKviz;
    }

    public Integer getIdKvizPovijest() {
        return idKvizPovijest;
    }

    public void setIdKvizPovijest(Integer idKvizPovijest) {
        this.idKvizPovijest = idKvizPovijest;
    }

    public NastavnaCjelina getNastavnaCjelina() {
        return nastavnaCjelina;
    }

    public void setNastavnaCjelina(NastavnaCjelina nastavnaCjelina) {
        this.nastavnaCjelina = nastavnaCjelina;
    }
}
