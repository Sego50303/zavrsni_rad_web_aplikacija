package Zavrsni_rad.web_aplikacija.domain;

import java.io.Serializable;

public class RezultatStudentKey implements Serializable {
    private int idObavljenTest;
    private int idPitanjePovijest;
    private int idStudent;
}
