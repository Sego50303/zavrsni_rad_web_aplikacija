package Zavrsni_rad.web_aplikacija.domain;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="razina_studij")
public class RazinaStudij {

    @Id
    private int idRazinaStudij;
    private String nazRazStud;

    public RazinaStudij() {}

    public RazinaStudij(int idRazinaStudij, String nazRazStud) {
        this.idRazinaStudij = idRazinaStudij;
        this.nazRazStud = nazRazStud;
    }

    public int getIdRazinaStudij() {
        return idRazinaStudij;
    }

    public void setIdRazinaStudij(int idRazinaStudij) {
        this.idRazinaStudij = idRazinaStudij;
    }

    public String getNazRazStud() {
        return nazRazStud;
    }

    public void setNazRazStud(String nazRazStud) {
        this.nazRazStud = nazRazStud;
    }
}
