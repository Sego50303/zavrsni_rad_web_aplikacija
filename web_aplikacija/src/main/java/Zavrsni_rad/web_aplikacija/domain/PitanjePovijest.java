package Zavrsni_rad.web_aplikacija.domain;

import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;

import javax.persistence.*;
import java.util.List;

@Entity
@Table(name="pitanje_povijest")
public class PitanjePovijest {
    @Id
    private int idPitanjePovijest;
    @Column(unique = true)
    private int idKvizPovijest;
    private String tekstPitanje;
    private float bodToc;
    private float bodNeToc;
    private float bodNeOdg;

    @ManyToOne
    @JoinColumn(name = "idKvizPovijest", insertable = false, updatable = false,
            referencedColumnName = "idKvizPovijest", nullable = false)
    private KvizPovijest kvizPovijest;

    @OneToMany(mappedBy = "pitanjePovijest")
    @Fetch(FetchMode.JOIN)
    private List<OdgovorPovijest> odgovoriPovijest;

    public PitanjePovijest() {}

    public PitanjePovijest(int idKvizPovijest, String tekstPitanje, float bodToc, float bodNeToc, float bodNeOdg) {
        this.idKvizPovijest = idKvizPovijest;
        this.tekstPitanje = tekstPitanje;
        this.bodToc = bodToc;
        this.bodNeToc = bodNeToc;
        this.bodNeOdg = bodNeOdg;
    }

    public PitanjePovijest(int idPitanjePovijest, int idKvizPovijest, String tekstPitanje, float bodToc, float bodNeToc, float bodNeOdg) {
        this.idPitanjePovijest = idPitanjePovijest;
        this.idKvizPovijest = idKvizPovijest;
        this.tekstPitanje = tekstPitanje;
        this.bodToc = bodToc;
        this.bodNeToc = bodNeToc;
        this.bodNeOdg = bodNeOdg;
    }

    public int getIdPitanjePovijest() {
        return idPitanjePovijest;
    }

    public void setIdPitanjePovijest(int idPitanjePovijest) {
        this.idPitanjePovijest = idPitanjePovijest;
    }

    public int getIdKvizPovijest() {
        return idKvizPovijest;
    }

    public void setIdKvizPovijest(int idKvizPovijest) {
        this.idKvizPovijest = idKvizPovijest;
    }

    public String getTekstPitanje() {
        return tekstPitanje;
    }

    public void setTekstPitanje(String tekstPitanje) {
        this.tekstPitanje = tekstPitanje;
    }

    public float getBodToc() {
        return bodToc;
    }

    public void setBodToc(float bodToc) {
        this.bodToc = bodToc;
    }

    public float getBodNeToc() {
        return bodNeToc;
    }

    public void setBodNeToc(float bodNeToc) {
        this.bodNeToc = bodNeToc;
    }

    public float getBodNeOdg() {
        return bodNeOdg;
    }

    public void setBodNeOdg(float bodNeOdg) {
        this.bodNeOdg = bodNeOdg;
    }

    public KvizPovijest getKvizPovijest() {
        return kvizPovijest;
    }

    public void setKvizPovijest(KvizPovijest kvizPovijest) {
        this.kvizPovijest = kvizPovijest;
    }

    public List<OdgovorPovijest> getOdgovoriPovijest() {
        return odgovoriPovijest;
    }

    public void setOdgovoriPovijest(List<OdgovorPovijest> odgovoriPovijest) {
        this.odgovoriPovijest = odgovoriPovijest;
    }
}
