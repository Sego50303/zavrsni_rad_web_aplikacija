package Zavrsni_rad.web_aplikacija.domain;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="student")
public class Student {
    @Id
    private int idStudent;
    private String imeStudent;
    private String prezimeStudent;

    public Student() {}

    public Student(int idStudent, String imeStudent, String prezimeStudent) {
        this.idStudent = idStudent;
        this.imeStudent = imeStudent;
        this.prezimeStudent = prezimeStudent;
    }

    public int getIdStudent() {
        return idStudent;
    }

    public void setIdStudent(int idStudent) {
        this.idStudent = idStudent;
    }

    public String getImeStudent() {
        return imeStudent;
    }

    public void setImeStudent(String imeStudent) {
        this.imeStudent = imeStudent;
    }

    public String getPrezimeStudent() {
        return prezimeStudent;
    }

    public void setPrezimeStudent(String prezimeStudent) {
        this.prezimeStudent = prezimeStudent;
    }
}
