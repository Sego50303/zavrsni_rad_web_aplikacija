package Zavrsni_rad.web_aplikacija.domain;

import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;

import javax.persistence.*;

@Entity
@Table(name="nastavna_cjelina")
public class NastavnaCjelina {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int idNastavnaCjelina;
    @Column(name = "nazivNastCjel")
    private String nazNastCjel;
    private String kratkiOpisNastCjel;
    private String opisNastCjel;
    private int idPredmet;
    private int redniBrojNastCjel;

    //@ManyToOne
    //@JoinColumn(name = "idPredmet", insertable = false, updatable = false,
    //        referencedColumnName = "idPredmet", nullable = false)
    //@Fetch(FetchMode.JOIN)
    //private Predmet predmet;

    public NastavnaCjelina(){}

    public NastavnaCjelina(String nazNastCjel, String kratkiOpisNastCjel, String opisNastCjel, int idPredmet, int redniBrojNastCjel) {
        this.nazNastCjel = nazNastCjel;
        this.kratkiOpisNastCjel = kratkiOpisNastCjel;
        this.opisNastCjel = opisNastCjel;
        this.idPredmet = idPredmet;
        this.redniBrojNastCjel = redniBrojNastCjel;
    }

    public NastavnaCjelina(int idNastavnaCjelina, String nazNastCjel, String kratkiOpisNastCjel, String opisNastCjel, int idpredmet, int redniBrojNastCjel) {
        this.idNastavnaCjelina = idNastavnaCjelina;
        this.nazNastCjel = nazNastCjel;
        this.kratkiOpisNastCjel = kratkiOpisNastCjel;
        this.opisNastCjel = opisNastCjel;
        this.idPredmet = idpredmet;
        this.redniBrojNastCjel = redniBrojNastCjel;
    }

    public int getIdNastavnaCjelina() {
        return idNastavnaCjelina;
    }

    public void setIdNastavnaCjelina(int idNastavnaCjelina) {
        this.idNastavnaCjelina = idNastavnaCjelina;
    }

    public String getNazNastCjel() {
        return nazNastCjel;
    }

    public void setNazNastCjel(String nazNastCjel) {
        this.nazNastCjel = nazNastCjel;
    }

    public String getKratkiOpisNastCjel() {
        return kratkiOpisNastCjel;
    }

    public void setKratkiOpisNastCjel(String kratkiOpisNastCjel) {
        this.kratkiOpisNastCjel = kratkiOpisNastCjel;
    }

    public String getOpisNastCjel() {
        return opisNastCjel;
    }

    public void setOpisNastCjel(String opisNastCjel) {
        this.opisNastCjel = opisNastCjel;
    }

    public int getIdPredmet() {
        return idPredmet;
    }

    public void setIdPredmet(int idPredmet) {
        this.idPredmet = idPredmet;
    }

    public int getRedniBrojNastCjel() {
        return redniBrojNastCjel;
    }

    public void setRedniBrojNastCjel(int redniBrojNastCjel) {
        this.redniBrojNastCjel = redniBrojNastCjel;
    }
}
