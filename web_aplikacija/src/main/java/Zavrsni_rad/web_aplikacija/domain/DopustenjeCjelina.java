package Zavrsni_rad.web_aplikacija.domain;

import javax.persistence.*;

@Entity
@Table(name="dopustenje_cjelina")
@IdClass(DopustenjeCjelinaKey.class)
public class DopustenjeCjelina {
    @Id
    private int idKorisnik;
    @Id
    @Column(name = "idNastCjel")
    private int idNastavnaCjelina;
    private char dopustenje;

    public DopustenjeCjelina() {}

    public DopustenjeCjelina(int idKorisnik, int idNastCjel, char dopustenje) {
        this.idKorisnik = idKorisnik;
        this.idNastavnaCjelina = idNastCjel;
        this.dopustenje = dopustenje;
    }

    public int getIdKorisnik() {
        return idKorisnik;
    }

    public void setIdKorisnik(int idKorisnik) {
        this.idKorisnik = idKorisnik;
    }

    public int getIdNastCjel() {
        return idNastavnaCjelina;
    }

    public void setIdNastCjel(int idNastCjel) {
        this.idNastavnaCjelina = idNastCjel;
    }

    public char getDopustenje() {
        return dopustenje;
    }

    public void setDopustenje(char dopustenje) {
        this.dopustenje = dopustenje;
    }
}
