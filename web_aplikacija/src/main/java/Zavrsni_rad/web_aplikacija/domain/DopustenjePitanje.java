package Zavrsni_rad.web_aplikacija.domain;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.Table;
import java.io.Serializable;

@Entity
@Table(name="dopustenje_pitanje")
@IdClass(DopustenjePitanjeKey.class)
public class DopustenjePitanje {
    @Id
    private int idKorisnik;
    @Id
    private int idPitanje;
    private char dopustenje;

    public DopustenjePitanje() {}

    public DopustenjePitanje(int idKorisnik, int idPitanje, char dopustenje) {
        this.idKorisnik = idKorisnik;
        this.idPitanje = idPitanje;
        this.dopustenje = dopustenje;
    }

    public int getIdKorisnik() {
        return idKorisnik;
    }

    public void setIdKorisnik(int idKorisnik) {
        this.idKorisnik = idKorisnik;
    }

    public int getIdPitanje() {
        return idPitanje;
    }

    public void setIdPitanje(int idPitanje) {
        this.idPitanje = idPitanje;
    }

    public char getDopustenje() {
        return dopustenje;
    }

    public void setDopustenje(char dopustenje) {
        this.dopustenje = dopustenje;
    }
}
