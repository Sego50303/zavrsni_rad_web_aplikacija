package Zavrsni_rad.web_aplikacija.domain;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="vrsta_pitanje")
public class VrstaPitanje {
    @Id
    private char idVrstaPitanje;
    private String znacenje;

    public VrstaPitanje() {}

    public VrstaPitanje(char idVrstaPitanje, String znacenje) {
        this.idVrstaPitanje = idVrstaPitanje;
        this.znacenje = znacenje;
    }

    public char getIdVrstaPitanje() {
        return idVrstaPitanje;
    }

    public void setIdVrstaPitanje(char idVrstaPitanje) {
        this.idVrstaPitanje = idVrstaPitanje;
    }

    public String getZnacenje() {
        return znacenje;
    }

    public void setZnacenje(String znacenje) {
        this.znacenje = znacenje;
    }
}
