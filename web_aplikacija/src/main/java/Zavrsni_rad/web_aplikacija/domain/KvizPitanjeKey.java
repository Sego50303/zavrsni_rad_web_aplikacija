package Zavrsni_rad.web_aplikacija.domain;

import java.io.Serializable;

public class KvizPitanjeKey implements Serializable {
    private int idKviz;
    private int idPitanje;

    public KvizPitanjeKey(){}

    public KvizPitanjeKey(int idKviz, int idPitanje) {
        this.idKviz = idKviz;
        this.idPitanje = idPitanje;
    }

    public int getIdKviz() {
        return idKviz;
    }

    public void setIdKviz(int idKviz) {
        this.idKviz = idKviz;
    }

    public int getIdPitanje() {
        return idPitanje;
    }

    public void setIdPitanje(int idPitanje) {
        this.idPitanje = idPitanje;
    }
}
