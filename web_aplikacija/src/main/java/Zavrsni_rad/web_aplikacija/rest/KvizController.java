package Zavrsni_rad.web_aplikacija.rest;

import Zavrsni_rad.web_aplikacija.domain.DopustenjeKviz;
import Zavrsni_rad.web_aplikacija.domain.Kviz;
import Zavrsni_rad.web_aplikacija.service.DopustenjeKvizService;
import Zavrsni_rad.web_aplikacija.service.KvizService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@CrossOrigin(origins = "http://localhost:4200")
@RestController
@RequestMapping("/kviz")
public class KvizController {
    @Autowired
    private KvizService kvizService;
    @Autowired
    private DopustenjeKvizService dopustenjeKvizService;

    @PostMapping("/{idKorisnik}")
    public Kviz createKviz(@PathVariable int idKorisnik, @RequestBody Kviz kviz){
        Kviz k =  kvizService.saveKviz(new Kviz(kviz.getIdNastCjel(),
                kviz.getImeKviz(), kviz.getKratkiOpisKviz(), kviz.getOpisKviz(),
                kviz.getRedniBrojKviz()));
        dopustenjeKvizService.saveDopustenje(new DopustenjeKviz(idKorisnik, k.getIdKviz(), 'o'));
        return k;
    }

    @GetMapping("/{idKviz}")
    public Kviz getKvizById(@PathVariable int idKviz){
        return kvizService.getKvizById(idKviz);
    }

    @DeleteMapping("/{idKviz}")
    public void deleteKviz(@PathVariable int idKviz){
        kvizService.deleteKviz(idKviz);
    }

    @GetMapping("/cjelina/{idNastavnaCjelina}/{idKorisnik}")
    public List<Kviz> getKvizByIdNastCjel(@PathVariable int idNastavnaCjelina, @PathVariable int idKorisnik){
        return kvizService.getKvizByIdNastCjelCustom(idNastavnaCjelina, idKorisnik);
    }

    @PutMapping("")
    public Kviz updateKviz(@RequestBody Kviz kviz){
        return this.kvizService.saveKviz(kviz);
    }

    @GetMapping("/naziv/{imeKviz}/{idKorisnik}")
    public Kviz[] searchKviz(@PathVariable String imeKviz, @PathVariable int idKorisnik){
        return kvizService.findByImeKviz(imeKviz, idKorisnik);
    }
}
