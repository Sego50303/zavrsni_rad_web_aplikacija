package Zavrsni_rad.web_aplikacija.rest;

import Zavrsni_rad.web_aplikacija.domain.DopustenjeCjelina;
import Zavrsni_rad.web_aplikacija.domain.NastavnaCjelina;
import Zavrsni_rad.web_aplikacija.service.DopustenjeCjelinaService;
import Zavrsni_rad.web_aplikacija.service.NastavnaCjelinaService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@CrossOrigin(origins = "http://localhost:4200")
@RestController
@RequestMapping("/nastavnaCjelina")
public class NastavnaCjelinaController {
    @Autowired
    private NastavnaCjelinaService nastavnaCjelinaService;
    @Autowired
    private DopustenjeCjelinaService dopustenjeCjelinaService;

    @GetMapping("/{idPredmet}/{idKorisnik}")
    public List<NastavnaCjelina> findByIdPredmet(@PathVariable int idPredmet, @PathVariable int idKorisnik){
        return nastavnaCjelinaService.findByIdPredmetCustom(idPredmet, idKorisnik);
    }

    @PutMapping("")
    public NastavnaCjelina updateNastavnaCjelina(@RequestBody NastavnaCjelina nastavnaCjelina){
        return nastavnaCjelinaService.saveNastavnaCjelina(nastavnaCjelina);
    }

    @PostMapping("/{idKorisnik}")
    public NastavnaCjelina createNastavnaCjelina(@PathVariable int idKorisnik, @RequestBody NastavnaCjelina nastavnaCjelina){
        //System.out.println("IdPredmet = " + nastavnaCjelina.getIdPredmet());
        NastavnaCjelina n = nastavnaCjelinaService.saveNastavnaCjelina(
                new NastavnaCjelina(nastavnaCjelina.getNazNastCjel(), nastavnaCjelina.getKratkiOpisNastCjel(),
                        nastavnaCjelina.getOpisNastCjel(), nastavnaCjelina.getIdPredmet(),
                        nastavnaCjelina.getRedniBrojNastCjel())
        );
        //System.out.println("IdNastavnaCjelina = " + n.getIdNastavnaCjelina());
        dopustenjeCjelinaService.saveDopustenje(new DopustenjeCjelina(idKorisnik, n.getIdNastavnaCjelina(), 'o'));

        return n;
    }

    @DeleteMapping("/{id}")
    public void deleteNastavnaCjelina(@PathVariable int id){
        nastavnaCjelinaService.deleteNastavnaCjelina(id);
    }

    @GetMapping("/naziv/{nazNastCjel}/{idKorisnik}")
    public List<NastavnaCjelina> findByNazNastCjel(@PathVariable String nazNastCjel, @PathVariable int idKorisnik){
        return nastavnaCjelinaService.findByNazNastCjel(nazNastCjel, idKorisnik);
    }

    @GetMapping("/cjelina/{idNastavnaCjelina}")
    public NastavnaCjelina findById(@PathVariable int idNastavnaCjelina){
        return nastavnaCjelinaService.findByIdNastavnaCjelina(idNastavnaCjelina);
    }
}
