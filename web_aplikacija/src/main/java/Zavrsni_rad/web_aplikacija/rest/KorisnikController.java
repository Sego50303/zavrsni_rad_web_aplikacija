package Zavrsni_rad.web_aplikacija.rest;

import Zavrsni_rad.web_aplikacija.domain.Korisnik;
import Zavrsni_rad.web_aplikacija.service.KorisnikService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

//@CrossOrigin(origins ="http://localhost:4200")
@RestController
@RequestMapping("/korisnik")
public class KorisnikController {
    @Autowired
    private KorisnikService korisnikService;

    @GetMapping("")
    public List<Korisnik> listKorisnik(){
        return korisnikService.listAll();
    }

    @GetMapping("/{imeKorisnik}")
    public Integer imeKorisnikToIdKorisnik(@PathVariable String imeKorisnik){
        return korisnikService.imeKorisnikToIdKorisnik(imeKorisnik);
    }
}
