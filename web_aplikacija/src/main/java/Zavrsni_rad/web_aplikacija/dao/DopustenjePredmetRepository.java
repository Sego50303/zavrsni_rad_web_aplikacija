package Zavrsni_rad.web_aplikacija.dao;

import Zavrsni_rad.web_aplikacija.domain.DopustenjePredmet;
import Zavrsni_rad.web_aplikacija.domain.DopustenjePredmetKey;
import Zavrsni_rad.web_aplikacija.domain.Predmet;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface DopustenjePredmetRepository extends JpaRepository<DopustenjePredmet, DopustenjePredmetKey> {
    @Query("SELECT DISTINCT p FROM Predmet p LEFT JOIN DopustenjePredmet dp ON p.idPredmet = dp.idPredmet " +
            "WHERE dp.idKorisnik=(:idKorisnik)")
    List<Predmet> predmetZaKoristenje(@Param("idKorisnik") Integer idKorisnik);

}
