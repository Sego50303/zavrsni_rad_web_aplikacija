package Zavrsni_rad.web_aplikacija.dao;

import Zavrsni_rad.web_aplikacija.domain.RezultatStudent;
import Zavrsni_rad.web_aplikacija.domain.RezultatStudentKey;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

@Repository
public interface RezultatStudentRepository extends JpaRepository<RezultatStudent, RezultatStudentKey> {
    @Query("SELECT COUNT(DISTINCT rs.idStudent) FROM RezultatStudent rs" +
            " WHERE rs.idObavljenTest=(:idObavljenTest)")
    int getBrStud(@Param("idObavljenTest") int idObavljenTest);

    @Query("SELECT COALESCE(SUM(pp.bodToc), 0) FROM RezultatStudent rs" +
            " JOIN PitanjePovijest pp" +
            " ON pp.idPitanjePovijest=rs.idPitanjePovijest" +
            " WHERE tocnostOdg=1" +
            " AND rs.idObavljenTest=(:idObavljenTest)")
    int getTocniBodovi(@Param("idObavljenTest") int idObavljenTest);

    @Query("SELECT COALESCE(SUM(pp.bodNeToc), 0) FROM RezultatStudent rs" +
            " JOIN PitanjePovijest pp" +
            " ON pp.idPitanjePovijest=rs.idPitanjePovijest" +
            " WHERE tocnostOdg=0" +
            " AND rs.idObavljenTest=(:idObavljenTest)")
    int getNetocniBodovi(@Param("idObavljenTest") int idObavljenTest);

    @Query("SELECT COALESCE(SUM(pp.bodNeOdg), 0) FROM RezultatStudent rs" +
            " JOIN PitanjePovijest pp" +
            " ON pp.idPitanjePovijest=rs.idPitanjePovijest" +
            " WHERE tocnostOdg IS NULL" +
            " AND rs.idObavljenTest=(:idObavljenTest)")
    int getNeodgovoreniBodovi(@Param("idObavljenTest") int idObavljenTest);

    @Query("SELECT COUNT(DISTINCT rs.idStudent) FROM RezultatStudent rs" +
            " WHERE rs.idObavljenTest=(:idObavljenTest)" +
            " AND rs.idPitanjePovijest=(:idPitanjePovijest)" +
            " AND rs.tocnostOdg=1")
    int getBrStudTocnoOdg(@Param("idObavljenTest") int idObavljenTest,@Param("idPitanjePovijest") int idPitanjePovijest);
}
