package Zavrsni_rad.web_aplikacija.dao;

import Zavrsni_rad.web_aplikacija.domain.OdgovorPovijest;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface OdgovorPovijestRepository extends JpaRepository<OdgovorPovijest,Integer> {
}
