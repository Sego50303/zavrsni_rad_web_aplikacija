package Zavrsni_rad.web_aplikacija.service;

import Zavrsni_rad.web_aplikacija.domain.NastavnaCjelina;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public interface NastavnaCjelinaService {
    List<NastavnaCjelina> listAll();

    List<NastavnaCjelina> findByIdPredmet(int idPredmet);

    NastavnaCjelina saveNastavnaCjelina(NastavnaCjelina nastavnaCjelina);

    void deleteNastavnaCjelina(int idNastavnaCjelina);

    List<NastavnaCjelina> findByNazNastCjel(String nazNastCjel, int idKorisnik);

    NastavnaCjelina findByIdNastavnaCjelina(int idNastavnaCjelina);

    List<NastavnaCjelina> findByIdPredmetCustom(int idPredmet, int idKorisnik);

    //NastavnaCjelina saveNastavnaCjelinaIDopustenje(int idKorisnik, NastavnaCjelina nastavnaCjelina);
}
