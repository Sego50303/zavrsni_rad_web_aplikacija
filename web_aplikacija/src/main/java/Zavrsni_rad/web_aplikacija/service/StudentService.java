package Zavrsni_rad.web_aplikacija.service;

import Zavrsni_rad.web_aplikacija.domain.Student;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public interface StudentService {
    List<Student> listAll();
}
