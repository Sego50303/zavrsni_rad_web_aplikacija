package Zavrsni_rad.web_aplikacija.service;

import Zavrsni_rad.web_aplikacija.domain.RazinaStudij;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public interface RazinaStudijService {

    List<RazinaStudij> getRazineStudij();
    RazinaStudij getRazinaStudijById(int idRazinaStudij);
}
