package Zavrsni_rad.web_aplikacija.service;

import Zavrsni_rad.web_aplikacija.domain.Pitanje;
import Zavrsni_rad.web_aplikacija.domain.PonudeniOdgovor;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public interface PitanjeService {
    List<Pitanje> listAll();

    List<Pitanje> getPitanja(int idKviz, int idKorisnik);

    Pitanje savePitanje(Pitanje pitanje);

    void removePitanjeFromKviz(int idPitanje, int idKviz);

    void saveKvizPitanje(int idPitanje, int idKviz);

    Pitanje getPitanje(int idPitanje);
}
