package Zavrsni_rad.web_aplikacija.service;

import Zavrsni_rad.web_aplikacija.domain.DopustenjeCjelina;
import org.springframework.stereotype.Service;

@Service
public interface DopustenjeCjelinaService {

    DopustenjeCjelina saveDopustenje(DopustenjeCjelina dopustenjeCjelina);
}
