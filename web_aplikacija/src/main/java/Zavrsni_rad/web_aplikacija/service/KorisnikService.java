package Zavrsni_rad.web_aplikacija.service;

import Zavrsni_rad.web_aplikacija.domain.Korisnik;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public interface KorisnikService {
    List<Korisnik> listAll();
    Integer imeKorisnikToIdKorisnik(String imeKorisnik);
}
