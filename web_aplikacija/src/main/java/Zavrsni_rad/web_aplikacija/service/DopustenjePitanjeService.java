package Zavrsni_rad.web_aplikacija.service;

import Zavrsni_rad.web_aplikacija.domain.DopustenjePitanje;
import org.springframework.stereotype.Service;

@Service
public interface DopustenjePitanjeService {
    DopustenjePitanje saveDopustenje(DopustenjePitanje dopustenjePitanje);
}
