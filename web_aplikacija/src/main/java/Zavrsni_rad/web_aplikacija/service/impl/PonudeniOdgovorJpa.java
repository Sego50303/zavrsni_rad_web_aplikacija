package Zavrsni_rad.web_aplikacija.service.impl;

import Zavrsni_rad.web_aplikacija.dao.PonudeniOdgovorRepository;
import Zavrsni_rad.web_aplikacija.domain.PonudeniOdgovor;
import Zavrsni_rad.web_aplikacija.service.PonudeniOdgovorService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class PonudeniOdgovorJpa implements PonudeniOdgovorService {

    @Autowired
    private PonudeniOdgovorRepository ponudeniOdgovorRepository;

    @Override
    public void deletePonudeniOdgovor(int idOdgovor) {
        this.ponudeniOdgovorRepository.deleteById(idOdgovor);
    }

    @Override
    public PonudeniOdgovor addPonudeniOdgovor(PonudeniOdgovor ponudeniOdgovor) {
        return ponudeniOdgovorRepository.saveAndFlush(ponudeniOdgovor);
    }

    @Override
    public List<PonudeniOdgovor> listAll() {
        return ponudeniOdgovorRepository.findAll();
    }
}
