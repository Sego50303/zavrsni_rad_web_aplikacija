package Zavrsni_rad.web_aplikacija.service.impl;

import Zavrsni_rad.web_aplikacija.dao.RezultatStudentRepository;
import Zavrsni_rad.web_aplikacija.service.RezultatStudentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class RezultatStudentJpa implements RezultatStudentService {
    @Autowired
    private RezultatStudentRepository rezultatStudentRepository;

    @Override
    public int getBrStudTocnoOdg(int idObavljenTest, int idPitanjePovijest) {
        return rezultatStudentRepository.getBrStudTocnoOdg(idObavljenTest, idPitanjePovijest);
    }

    @Override
    public int getNetocniBodovi(int idObavljenTest) {
        return rezultatStudentRepository.getNetocniBodovi(idObavljenTest);
    }

    @Override
    public int getNeodgovoreniBodovi(int idObavljenTest) {
        return rezultatStudentRepository.getNeodgovoreniBodovi(idObavljenTest);
    }

    @Override
    public int getTocniBodovi(int idObavljenTest) {
        return rezultatStudentRepository.getTocniBodovi(idObavljenTest);
    }

    @Override
    public int getBrStud(int idObavljenTest) {
        return rezultatStudentRepository.getBrStud(idObavljenTest);
    }
}
