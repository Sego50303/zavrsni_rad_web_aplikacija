package Zavrsni_rad.web_aplikacija.service.impl;

import Zavrsni_rad.web_aplikacija.dao.KvizPitanjeRepository;
import Zavrsni_rad.web_aplikacija.dao.PitanjeRepository;
import Zavrsni_rad.web_aplikacija.domain.KvizPitanje;
import Zavrsni_rad.web_aplikacija.domain.KvizPitanjeKey;
import Zavrsni_rad.web_aplikacija.domain.Pitanje;
import Zavrsni_rad.web_aplikacija.domain.PonudeniOdgovor;
import Zavrsni_rad.web_aplikacija.service.PitanjeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class PitanjeJpa implements PitanjeService {
    @Autowired
    private PitanjeRepository pitanjeRepository;
    @Autowired
    private KvizPitanjeRepository kvizPitanjeRepository;

    @Override
    public Pitanje getPitanje(int idPitanje) {
        Optional<Pitanje> p = pitanjeRepository.findById(idPitanje);
        if(p.isPresent())
            return p.get();
        else
            return null;
    }

    @Override
    public void saveKvizPitanje(int idPitanje, int idKviz) {
        kvizPitanjeRepository.save(new KvizPitanje(idKviz,idPitanje,1));
    }

    @Override
    public void removePitanjeFromKviz(int idPitanje, int idKviz) {
        kvizPitanjeRepository.deleteById(new KvizPitanjeKey(idKviz, idPitanje));
    }

    @Override
    public Pitanje savePitanje(Pitanje pitanje) {
        return pitanjeRepository.saveAndFlush(pitanje);
    }

    @Override
    public List<Pitanje> listAll(){
        return pitanjeRepository.findAll();
    }

    @Override
    public List<Pitanje> getPitanja(int idKviz, int idKorisnik) {
        return pitanjeRepository.findByIdKvizCustom(idKviz, idKorisnik);
    }
}
