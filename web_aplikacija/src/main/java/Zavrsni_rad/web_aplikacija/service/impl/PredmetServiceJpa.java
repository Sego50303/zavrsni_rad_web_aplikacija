package Zavrsni_rad.web_aplikacija.service.impl;

import Zavrsni_rad.web_aplikacija.dao.DopustenjePredmetRepository;
import Zavrsni_rad.web_aplikacija.dao.PredmetRepository;
import Zavrsni_rad.web_aplikacija.domain.Predmet;
import Zavrsni_rad.web_aplikacija.service.PredmetService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class PredmetServiceJpa implements PredmetService {
    @Autowired
    private PredmetRepository predmetRepository;
    @Autowired
    private DopustenjePredmetRepository dopustenjePredmetRepository;

    @Override
    public void deletePredmet(int idPredmet) {
        predmetRepository.deleteById(idPredmet);
    }

    @Override
    public List<Predmet> findByNazivPredmet(String nazivPredmet, int idKorisnik) {
        return predmetRepository.findByNazivPredmet(nazivPredmet, idKorisnik);
    }

    @Override
    public Predmet savePredmet(Predmet predmet) {
        return predmetRepository.save(predmet);
    }

    @Override
    public Predmet getPredmet(int idPredmet) {
        return predmetRepository.findByIdPredmet(idPredmet);
    }

    @Override
    public List<Predmet> listAll(){
        return predmetRepository.findAll();
    }

    @Override
    public List<Predmet> listPredmet(int idKorisnik) {
        return dopustenjePredmetRepository.predmetZaKoristenje(idKorisnik);
    }
}
