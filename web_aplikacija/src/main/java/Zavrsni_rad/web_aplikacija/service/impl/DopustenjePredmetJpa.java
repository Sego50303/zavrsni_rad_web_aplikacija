package Zavrsni_rad.web_aplikacija.service.impl;

import Zavrsni_rad.web_aplikacija.dao.DopustenjePredmetRepository;
import Zavrsni_rad.web_aplikacija.domain.DopustenjePredmet;
import Zavrsni_rad.web_aplikacija.domain.Predmet;
import Zavrsni_rad.web_aplikacija.service.DopustenjePredmetService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class DopustenjePredmetJpa implements DopustenjePredmetService {
    @Autowired
    private DopustenjePredmetRepository dopustenjePredmetRepository;

    @Override
    public List<DopustenjePredmet> listAll(){
        return dopustenjePredmetRepository.findAll();
    }

    @Override
    public DopustenjePredmet saveDopustenje(DopustenjePredmet dopustenjePredmet) {
        return dopustenjePredmetRepository.save(dopustenjePredmet);
    }

    @Override
    public List<Predmet> predmetZaKoristenje(Integer idKorisnik) {
        return dopustenjePredmetRepository.predmetZaKoristenje(idKorisnik);
    }


}
