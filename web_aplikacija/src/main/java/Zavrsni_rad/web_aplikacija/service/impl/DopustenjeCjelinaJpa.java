package Zavrsni_rad.web_aplikacija.service.impl;

import Zavrsni_rad.web_aplikacija.dao.DopustenjeCjelinaRepository;
import Zavrsni_rad.web_aplikacija.domain.DopustenjeCjelina;
import Zavrsni_rad.web_aplikacija.service.DopustenjeCjelinaService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class DopustenjeCjelinaJpa implements DopustenjeCjelinaService {
    @Autowired
    private DopustenjeCjelinaRepository dopustenjeCjelinaRepository;

    @Override
    public DopustenjeCjelina saveDopustenje(DopustenjeCjelina dopustenjeCjelina) {
        return dopustenjeCjelinaRepository.save(dopustenjeCjelina);
    }
}
