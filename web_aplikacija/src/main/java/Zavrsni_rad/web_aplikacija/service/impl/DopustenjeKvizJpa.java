package Zavrsni_rad.web_aplikacija.service.impl;

import Zavrsni_rad.web_aplikacija.dao.DopustenjeKvizRepository;
import Zavrsni_rad.web_aplikacija.domain.DopustenjeKviz;
import Zavrsni_rad.web_aplikacija.service.DopustenjeKvizService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class DopustenjeKvizJpa implements DopustenjeKvizService {
    @Autowired
    private DopustenjeKvizRepository dopustenjeKvizRepository;

    @Override
    public DopustenjeKviz saveDopustenje(DopustenjeKviz dopustenjeKviz) {
        return dopustenjeKvizRepository.save(dopustenjeKviz);
    }
}
