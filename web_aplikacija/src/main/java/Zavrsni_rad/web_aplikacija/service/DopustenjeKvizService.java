package Zavrsni_rad.web_aplikacija.service;

import Zavrsni_rad.web_aplikacija.domain.DopustenjeKviz;
import org.springframework.stereotype.Service;

@Service
public interface DopustenjeKvizService {
    DopustenjeKviz saveDopustenje(DopustenjeKviz dopustenjeKviz);
}
