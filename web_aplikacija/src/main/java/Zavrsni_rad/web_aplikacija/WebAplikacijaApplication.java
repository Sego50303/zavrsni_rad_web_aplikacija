package Zavrsni_rad.web_aplikacija;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class WebAplikacijaApplication {

	public static void main(String[] args) {
		SpringApplication.run(WebAplikacijaApplication.class, args);
	}

}
