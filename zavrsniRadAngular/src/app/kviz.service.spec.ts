import { TestBed } from '@angular/core/testing';

import { KvizService } from './kviz.service';

describe('KvizService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: KvizService = TestBed.get(KvizService);
    expect(service).toBeTruthy();
  });
});
