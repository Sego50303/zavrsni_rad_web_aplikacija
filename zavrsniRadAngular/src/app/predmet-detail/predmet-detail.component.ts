import { Component, OnInit, Input, TemplateRef } from '@angular/core';
import { Predmet } from '../predmet';
import { ActivatedRoute } from '@angular/router'
import { Location } from '@angular/common'
import { PredmetService} from '../predmet.service'
import { RazinaStudij } from '../razinaStudij';
import { BsModalService, BsModalRef } from 'ngx-bootstrap/modal';

@Component({
  selector: 'app-predmet-detail',
  templateUrl: './predmet-detail.component.html',
  styleUrls: ['./predmet-detail.component.css']
})
export class PredmetDetailComponent implements OnInit {
  @Input() predmet : Predmet;
  razine: RazinaStudij[];
  modalRef: BsModalRef;

  constructor(private route: ActivatedRoute,
    private predmetService: PredmetService,
    private location: Location,
    private modalService: BsModalService) { }

  ngOnInit() {
    this.getPredmet();
    this.getRazine();
  }

  getPredmet(): void{
    const id = +this.route.snapshot.paramMap.get('id');
    this.predmetService.getPredmet(id).subscribe(predmet => this.predmet=predmet);
  }

  getRazine(){
    this.predmetService.getRazine().subscribe(razine => this.razine=razine);
  }

  goBack(): void{
    this.location.back();
  }

  save(): void {
    this.predmetService.updatePredmet(this.predmet)
      .subscribe(() => this.goBack());
  }

  delete(): void{
      this.predmetService.deletePredmet(this.predmet).subscribe( predmet => { 
        this.modalRef.hide();
        this.location.back();
       });
  }

  openModal(template: TemplateRef<any>) {
    this.modalRef = this.modalService.show(template);
  }

  closeModal(){
    this.modalRef.hide();
  }
}
