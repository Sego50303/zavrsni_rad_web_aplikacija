export class PitanjePovijest{
    idPitanjePovijest: number;
    idKvizPovijest: number;
    tekstPitanje: string;
    bodToc: number;
    bodNeToc: number;
    bodNeOdg: number;
}