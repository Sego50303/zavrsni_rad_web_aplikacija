import { Component, OnInit } from '@angular/core';
import { PomPorukeService } from '../pom-poruke.service'

@Component({
  selector: 'app-pom-poruke',
  templateUrl: './pom-poruke.component.html',
  styleUrls: ['./pom-poruke.component.css']
})
export class PomPorukeComponent implements OnInit {

  constructor(public pomPorukeService: PomPorukeService) { }

  ngOnInit() {
  }

}
