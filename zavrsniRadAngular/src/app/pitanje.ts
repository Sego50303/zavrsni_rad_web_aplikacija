export class Pitanje{
    idPitanje: number;
    idVrstaPitanje: string;
    tekstPitanje: string;
    bodToc: number;
    bodNeToc: number;
    bodNeOdg: number;
    temaPitanje: string;
}