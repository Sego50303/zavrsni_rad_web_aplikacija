import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NastavnaCjelinaDetailComponent } from './nastavna-cjelina-detail.component';

describe('NastavnaCjelinaDetailComponent', () => {
  let component: NastavnaCjelinaDetailComponent;
  let fixture: ComponentFixture<NastavnaCjelinaDetailComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NastavnaCjelinaDetailComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NastavnaCjelinaDetailComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
