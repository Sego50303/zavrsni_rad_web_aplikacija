import { Component, OnInit, Input, TemplateRef } from '@angular/core';
import { NastavnaCjelina } from '../nastavnaCjelina';
import { ActivatedRoute } from '@angular/router';
import { BsModalService, BsModalRef } from 'ngx-bootstrap/modal';
import { Location } from '@angular/common'
import { NastavnaCjelinaService } from '../nastavna-cjelina.service';

@Component({
  selector: 'app-nastavna-cjelina-detail',
  templateUrl: './nastavna-cjelina-detail.component.html',
  styleUrls: ['./nastavna-cjelina-detail.component.css']
})
export class NastavnaCjelinaDetailComponent implements OnInit {
  cjelina: NastavnaCjelina;
  modalRef: BsModalRef;

  constructor(private route: ActivatedRoute,private location: Location,
    private modalService: BsModalService, private cjelinaService: NastavnaCjelinaService) { }

  ngOnInit() {
    this.getCjelina();
  }


  goBack(): void{
    this.location.back();
  }

  save(): void {
    this.cjelinaService.updateNastavnaCjelina(this.cjelina).subscribe(()=> this.goBack());
  }

  openModal(template: TemplateRef<any>) {
    this.modalRef = this.modalService.show(template);
  }

  closeModal(){
    this.modalRef.hide();
  }

  getCjelina(){
    const id = +this.route.snapshot.paramMap.get('id');
    this.cjelinaService.getNastavnaCjelina(id).subscribe(cjelina=> this.cjelina=cjelina);
  }

  delete(): void{
    this.cjelinaService.deleteNastavnaCjelina(this.cjelina).subscribe( cjelina => { 
      this.modalRef.hide();
      this.location.back();
     });
  }
}
