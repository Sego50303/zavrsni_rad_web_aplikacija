import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';
import { NastavnaCjelina } from './nastavnaCjelina';
import { HttpHeaders, HttpClient } from '@angular/common/http';
import { PomPorukeService } from './pom-poruke.service';
import { tap } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class NastavnaCjelinaService {
  private nastavnaCjelinatURL='http://localhost:8080/nastavnaCjelina';
  httpOptions = {
    headers: new HttpHeaders({ 'Content-Type': 'application/json' })
  };
  private idKorisnik=1;

  constructor(private http: HttpClient, private pomPorukeService: PomPorukeService) { }

  getNastavneCjeline(idPredmet: number): Observable<NastavnaCjelina[]>{
    return this.http.get<NastavnaCjelina[]>(this.nastavnaCjelinatURL+"/"+idPredmet+"/"+this.idKorisnik,this.httpOptions)
  }

  searchNastavneCjeline(term: string): Observable<NastavnaCjelina[]>{
    if (!term.trim()) {
      return of([]);
    }
    return this.http.get<NastavnaCjelina[]>(this.nastavnaCjelinatURL+"/naziv/"+term+"/"+this.idKorisnik);
  }

  getNastavnaCjelina(idNastavnaCjelina: number): Observable<NastavnaCjelina>{
    return this.http.get<NastavnaCjelina>(this.nastavnaCjelinatURL+"/cjelina/"+idNastavnaCjelina);
  }

  updateNastavnaCjelina(cjelina: NastavnaCjelina): Observable<any> {
    return this.http.put<NastavnaCjelina>(this.nastavnaCjelinatURL, cjelina, this.httpOptions)
      .pipe(
        tap( _ => this.pomPorukeService.add(`Nastavna cjelina ${cjelina.nazNastCjel} ažurirana`))
      );
  }

  deleteNastavnaCjelina(cjelina: NastavnaCjelina): Observable<NastavnaCjelina>{
    return this.http.delete<NastavnaCjelina>(this.nastavnaCjelinatURL+"/"+cjelina.idNastavnaCjelina, this.httpOptions)
      .pipe(
        tap( _ => this.pomPorukeService.add(`Nastavna cjelina ${cjelina.nazNastCjel} izbrisana`))
      );
  }

  addNastavnaCjelina(cjelina: NastavnaCjelina): Observable<NastavnaCjelina>{
    //console.log(cjelina.idPredmet);
    return this.http.post<NastavnaCjelina>(this.nastavnaCjelinatURL+"/"+this.idKorisnik, cjelina, this.httpOptions)
      .pipe(
        tap( _ => this.pomPorukeService.add(`Nastavna cjelina ${cjelina.nazNastCjel} dodana`))
      );
  }
}
