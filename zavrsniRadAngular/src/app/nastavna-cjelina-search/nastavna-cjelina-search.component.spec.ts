import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NastavnaCjelinaSearchComponent } from './nastavna-cjelina-search.component';

describe('NastavnaCjelinaSearchComponent', () => {
  let component: NastavnaCjelinaSearchComponent;
  let fixture: ComponentFixture<NastavnaCjelinaSearchComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NastavnaCjelinaSearchComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NastavnaCjelinaSearchComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
