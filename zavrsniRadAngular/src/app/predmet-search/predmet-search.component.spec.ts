import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PredmetSearchComponent } from './predmet-search.component';

describe('PredmetSearchComponent', () => {
  let component: PredmetSearchComponent;
  let fixture: ComponentFixture<PredmetSearchComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PredmetSearchComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PredmetSearchComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
