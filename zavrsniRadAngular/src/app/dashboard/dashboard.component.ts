import { Component, OnInit, Input } from '@angular/core';
import { Kviz } from '../kviz';
import { Pitanje } from '../pitanje';
import { PitanjeService } from '../pitanje.service';
import { Observable } from 'rxjs';
import { PonudeniOdgovor } from '../ponudeniOdgovor';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css']
})
export class DashboardComponent implements OnInit {
  @Input() kviz : Kviz;
  pitanja : Pitanje[];

  novoPitanje: Pitanje={
    idPitanje: 0,
    idVrstaPitanje: '',
    tekstPitanje: '',
    bodToc: 0,
    bodNeToc: 0,
    bodNeOdg: 0,
    temaPitanje: ''
  }
  noviOdgovor: PonudeniOdgovor={
    idPonudeniOdgovor: 0,
    idPitanje: 0,
    tekstOdg: '',
    tocanOdg: false
  }

  pitanje: Pitanje={
    idPitanje: 0,
    idVrstaPitanje: '',
    tekstPitanje: '',
    bodToc: 0,
    bodNeToc: 0,
    bodNeOdg: 0,
    temaPitanje: ''
  }

  dodajPitanje: boolean = false;
  dodajOdgovor: number = 0;
  spremiPitanje: number = 0;
  spremljenoPitanje: number = 0;
 
  constructor(private pitanjeService: PitanjeService) { }
 
  ngOnInit() {
    this.getPitanja();
  }

  ngOnChanges(){
    this.getPitanja();
  }
 
  getPitanja(): void{
    this.pitanjeService.getPitanja(this.kviz.idKviz).subscribe(pitanja => {
      this.pitanja=pitanja;
    });
  }

  toggleDodajPitanje(){
    this.dodajPitanje=!this.dodajPitanje;
  }

  addPitanje(): void{
    this.pitanjeService.addPitanje(this.novoPitanje, this.kviz.idKviz).subscribe(pitanje =>{
      this.pitanja.unshift(pitanje);
      this.novoPitanje = {
        idPitanje: 0,
        idVrstaPitanje: '',
        tekstPitanje: '',
        bodToc: 0,
        bodNeToc: 0,
        bodNeOdg: 0,
        temaPitanje: ''
      }
      this.dodajPitanje=false;
    });
  }

  toggleSpremiPitanje(idPitanje: number): void{
    this.spremiPitanje=idPitanje;
    this.spremljenoPitanje=0;
  }

  toggleAddOdgovor(idPitanje: number): void{
    this.dodajOdgovor=idPitanje;
    this.noviOdgovor.tocanOdg=false;
  }

  toggleOdgovorTrue(state: boolean): void{
    this.noviOdgovor.tocanOdg=state;
  }

  savePitanje(pitanje: Pitanje): void{
    this.pitanjeService.savePitanje(pitanje).subscribe();
    this.toggleSpremiPitanje(0);
    this.spremljenoPitanje=pitanje.idPitanje;
  }

  addOdgovor(): void{
    this.noviOdgovor.idPitanje=this.dodajOdgovor;
    this.pitanjeService.addOdgovor(this.noviOdgovor).subscribe(odgovor =>{
      //this.pitanja[this.noviOdgovor.idPitanje].ponudeniOdgovori.unshift(odgovor);
      this.getPitanja();
      this.pitanjeService.getPitanje(this.pitanje.idPitanje).subscribe(pitanje => this.pitanje=pitanje);
      this.noviOdgovor = {
        idPonudeniOdgovor: 0,
        idPitanje: 0,
        tekstOdg: '',
        tocanOdg: false
      }
      this.dodajOdgovor=0;
    });
  }

  deleteOdgovor(odgovor: PonudeniOdgovor): void{
    this.pitanjeService.deleteOdgovor(odgovor).subscribe( _ => {
      this.getPitanja();
      this.pitanjeService.getPitanje(this.pitanje.idPitanje).subscribe(pitanje => this.pitanje=pitanje);
    });
  }

  deletePitanje(pitanje: Pitanje): void {
    this.pitanjeService.deletePitanje(pitanje, this.kviz.idKviz).subscribe( _ => {
      this.getPitanja();
      this.pitanje={
        idPitanje: 0,
        idVrstaPitanje: '',
        tekstPitanje: '',
        bodToc: 0,
        bodNeToc: 0,
        bodNeOdg: 0,
        temaPitanje: ''
      }
    });
  }

  changePitanje(pitanje: Pitanje){
    this.pitanje=pitanje;
  }
}