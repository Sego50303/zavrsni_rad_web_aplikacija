import { Component, OnInit } from '@angular/core';
import { RezultatiService } from '../rezultati.service';
import { ObavljenTest } from '../obavljenTest';
import { PitanjePovijest } from '../pitanjePovijest';
import { TestPom } from '../testPom';

@Component({
  selector: 'app-rezultati',
  templateUrl: './rezultati.component.html',
  styleUrls: ['./rezultati.component.css']
})
export class RezultatiComponent implements OnInit {
  obavljeniTestovi: ObavljenTest[];
  pitanja: Map<number,PitanjePovijest[]>;
  testPom: Map<number,TestPom>;
  trenutniTest=0;
  timeOptions = { year: 'numeric', month: 'numeric', day: 'numeric' };

  constructor(private rezultatiService: RezultatiService) { }

  ngOnInit() {
    this.pitanja = new Map();
    this.testPom = new Map();
    this.rezultatiService.getObavljeniTestovi().subscribe( testovi => this.obavljeniTestovi=testovi.reverse() );
  }


  getPitanja(test: ObavljenTest): void{
    if(!this.pitanja.has(test.idObavljenTest)){
      this.rezultatiService.getPitanja(test.idKvizPovijest).subscribe( pitanja => {
        this.pitanja.set(test.idObavljenTest,pitanja);
      });
      this.rezultatiService.getTestPom(test).subscribe( testInfo => {
        this.testPom.set(test.idObavljenTest, testInfo);
      });
    }
    this.trenutniTest=test.idObavljenTest;
  }
}
