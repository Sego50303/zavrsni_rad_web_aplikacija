import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class PomPorukeService {
  poruka: string = '';
  poruke: string[] = [];

  constructor() { }

  add(poruka: string){
    this.poruke.push(poruka);
    this.poruka=poruka;
  }

  clear(){
    this.poruke = [];
  }
}
