import { Component, OnInit } from '@angular/core';
import { Predmet } from '../predmet';
import { PredmetService } from '../predmet.service';
import { RazinaStudij } from '../razinaStudij';
import { NastavnaCjelina } from '../nastavnaCjelina';
import { NastavnaCjelinaService } from '../nastavna-cjelina.service';
import { Subject, Observable } from 'rxjs';
import { distinctUntilChanged, debounceTime, switchMap } from 'rxjs/operators';
import { Kviz } from '../kviz';
import { KvizService } from '../kviz.service';
import { PregledService } from '../pregled.service';

@Component({
  selector: 'app-pregled',
  templateUrl: './pregled.component.html',
  styleUrls: ['./pregled.component.css']
})
export class PregledComponent implements OnInit {
  trenutniPredmet : Predmet={
    idPredmet:0,
    nazivPredmet:'',
    idRazStud:0,
    nastProg:''
  };
  noviPredmet : Predmet={
    idPredmet:0,
    nazivPredmet:'',
    idRazStud:0,
    nastProg:''
  };
  novaCjelina: NastavnaCjelina={
    idNastavnaCjelina:0,
    nazNastCjel: '',
    kratkiOpisNastCjel: '',
    opisNastCjel: '',
    idPredmet: 0,
    redniBrojNastCjel: 0
  }
  trenutnaCjelina: NastavnaCjelina={
    idNastavnaCjelina:0,
    nazNastCjel: '',
    kratkiOpisNastCjel: '',
    opisNastCjel: '',
    idPredmet: 0,
    redniBrojNastCjel: 0
  }
  noviKviz: Kviz={
    idKviz: 0,
    idNastCjel: 0,
    imeKviz: '',
    kratkiOpisKviz: '',
    opisKviz: '',
    redniBrojKviz: 0,
    idKvizPovijest: null
  }
  trenutniKviz: Kviz={
    idKviz: 0,
    idNastCjel: 0,
    imeKviz: '',
    kratkiOpisKviz: '',
    opisKviz: '',
    redniBrojKviz: 0,
    idKvizPovijest: null
  }

  predmeti: Predmet[];
  cjeline: NastavnaCjelina[];
  kvizovi: Kviz[];
  prikazPredmeti: Predmet[];
  prikazCjeline: NastavnaCjelina[];
  prikazCjeline$: Observable<NastavnaCjelina[]>;
  prikazKvizovi: Kviz[];
  prikazKvizovi$: Observable<Kviz[]>;

  dodajPredmet: boolean=false;
  dodajCjelinu: boolean=false;
  dodajKviz: boolean=false;

  razine: RazinaStudij[];
  searchTermsCjelina = new Subject<string>();
  searchTermsKviz = new Subject<string>();
  
  constructor(private predmetService : PredmetService, private cjelinaService: NastavnaCjelinaService, private kvizService: KvizService, private pregledService: PregledService) {  }

  ngOnInit() {
    this.getPredmeti();
    this.getRazine();
    this.prikazCjeline$ = this.searchTermsCjelina.pipe(
      debounceTime(300), //milisekunde
      distinctUntilChanged(), //dok se ne promijeni
      switchMap((term : string) => this.cjelinaService.searchNastavneCjeline(term)), //pretrazi
    );
    this.prikazKvizovi$ = this.searchTermsKviz.pipe(
      debounceTime(300),    
      distinctUntilChanged(),
      switchMap((term: string) => this.kvizService.searchKviz(term)),
    );
    if(this.pregledService.getTrenutniPredmet().idPredmet!=0){
      this.trenutniPredmet=this.pregledService.getTrenutniPredmet();
      this.onSelectPredmet(this.trenutniPredmet)
    }
    if(this.pregledService.getTrenutnaCjelina().idNastavnaCjelina!=0){
      this.trenutnaCjelina=this.pregledService.getTrenutnaCjelina();
      this.onSelectCjelina(this.trenutnaCjelina);
    }
  }

  onSelectPredmet(predmet: Predmet): void{
    this.trenutniPredmet=predmet;
    this.cjelinaService.getNastavneCjeline(predmet.idPredmet).subscribe( cjeline =>{
      this.cjeline=cjeline; // zbog ove dvije linije, dogada se da su cjeline i prikazCjeline jedno te isto
      this.prikazCjeline=cjeline;
    });
    this.pregledService.setTrenutniPredmet(predmet);
  }

  onSelectCjelina(cjelina: NastavnaCjelina): void{
    this.trenutnaCjelina=cjelina;
    this.kvizService.getKvizovi(cjelina.idNastavnaCjelina).subscribe( kvizovi =>{
      this.kvizovi=kvizovi;
      this.prikazKvizovi=kvizovi;
    });
    this.pregledService.setTrenutnaCjelina(cjelina);
  }

  onSelectKviz(kviz: Kviz): void{
    this.trenutniKviz=kviz;
  }

  getPredmeti(): void {
    this.predmetService.getPredmeti().subscribe(predmeti => {
      this.predmeti=predmeti;
      this.prikazPredmeti=predmeti;
    });
  }

  addPredmet(): void {
    this.noviPredmet.nazivPredmet = this.noviPredmet.nazivPredmet.trim();
    if (!this.noviPredmet.nazivPredmet || !this.noviPredmet.nastProg || !this.noviPredmet.idRazStud) { return; }
    this.predmetService.addPredmet(this.noviPredmet)
      .subscribe(predmet => {
        this.predmeti.push(predmet);
        this.dodajPredmet=false;
        this.noviPredmet={
          idPredmet:0,
          nazivPredmet:'',
          idRazStud:0,
          nastProg:''
        }
      });
  }

  addCjelina(): void{
    if (!this.novaCjelina.nazNastCjel || !this.novaCjelina.kratkiOpisNastCjel || !this.novaCjelina.opisNastCjel) { return; }
    this.novaCjelina.idPredmet=this.trenutniPredmet.idPredmet;
    //this.novaCjelina.redniBrojNastCjel=0;
    this.cjelinaService.addNastavnaCjelina(this.novaCjelina).subscribe(cjelina=>{
      this.prikazCjeline.unshift(cjelina);
      //this.cjeline.push(cjelina);
      this.dodajCjelinu=false;
      this.novaCjelina={
        idNastavnaCjelina:0,
        nazNastCjel: '',
        kratkiOpisNastCjel: '',
        opisNastCjel: '',
        idPredmet: 0,
        redniBrojNastCjel: 0
      }
    });
  }

  addKviz(): void{
    if (!this.noviKviz.imeKviz || !this.noviKviz.kratkiOpisKviz || !this.noviKviz.opisKviz) { return; }
    this.noviKviz.idNastCjel=this.trenutnaCjelina.idNastavnaCjelina;
    this.kvizService.addKviz(this.noviKviz).subscribe(kviz=>{
      this.prikazKvizovi.unshift(kviz);
      this.dodajKviz=false;
      this.noviKviz={
        idKviz: 0,
        idNastCjel: 0,
        imeKviz: '',
        kratkiOpisKviz: '',
        opisKviz: '',
        redniBrojKviz: 0,
        idKvizPovijest: null
      }
    });
  }

  toggleDodajPredmet(){
    this.dodajPredmet=!this.dodajPredmet;
  }

  toggleDodajCjelinu(){
    this.dodajCjelinu=!this.dodajCjelinu;
  }

  toggleDodajKviz(){
    this.dodajKviz=!this.dodajKviz;
  }

  getRazine(){
    this.predmetService.getRazine().subscribe(razine => this.razine=razine);
  }

  filterPredmet(term: string): void{
    this.prikazPredmeti=this.predmetService.filter(term, this.predmeti);
  }

  searchCjeline(term: string): void {
    this.searchTermsCjelina.next(term);
  }

  searchKviz(term: string): void{
    this.searchTermsKviz.next(term);
  }
}
