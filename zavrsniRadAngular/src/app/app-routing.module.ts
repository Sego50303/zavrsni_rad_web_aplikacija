import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { PregledComponent } from './pregled/pregled.component'
import { PredmetDetailComponent } from './predmet-detail/predmet-detail.component'
import { NastavnaCjelinaDetailComponent } from './nastavna-cjelina-detail/nastavna-cjelina-detail.component';
import { KvizDetailComponent } from './kviz-detail/kviz-detail.component';
import { RezultatiComponent } from './rezultati/rezultati.component';

const routes: Routes = [
  { path: '', redirectTo: '/pregled', pathMatch: 'full' },
  { path: 'rezultati', component: RezultatiComponent },
  { path: 'detalj/:id', component: PredmetDetailComponent },
  { path: 'pregled', component: PregledComponent },
  { path: 'detaljCjelina/:id', component: NastavnaCjelinaDetailComponent },
  { path: 'detaljKviz/:id', component: KvizDetailComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
