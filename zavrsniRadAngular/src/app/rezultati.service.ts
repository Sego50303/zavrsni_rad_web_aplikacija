import { Injectable } from '@angular/core';
import { HttpHeaders, HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { ObavljenTest } from './obavljenTest';
import { PitanjePovijest } from './pitanjePovijest';
import { TestPom } from './testPom';

@Injectable({
  providedIn: 'root'
})
export class RezultatiService {
  private obavljenTesttURL='http://localhost:8080/obavljenTest';
  httpOptions = {
    headers: new HttpHeaders({ 'Content-Type': 'application/json' })
  };
  private idKorisnik=1;

  constructor(private http: HttpClient) { }

  getObavljeniTestovi(): Observable<ObavljenTest[]>{
    return this.http.get<ObavljenTest[]>(this.obavljenTesttURL+"/"+this.idKorisnik, this.httpOptions);
  }

  getPitanja(idKvizPovijest: number): Observable<PitanjePovijest[]>{
    return this.http.get<PitanjePovijest[]>(this.obavljenTesttURL+"/pitanja/"+idKvizPovijest, this.httpOptions);
  }

  getTestPom(test: ObavljenTest): Observable<TestPom>{
    return this.http.post<TestPom>(this.obavljenTesttURL+"/info", test, this.httpOptions);
  }
}
