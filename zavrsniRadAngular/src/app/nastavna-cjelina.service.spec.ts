import { TestBed } from '@angular/core/testing';

import { NastavnaCjelinaService } from './nastavna-cjelina.service';

describe('NastavnaCjelinaService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: NastavnaCjelinaService = TestBed.get(NastavnaCjelinaService);
    expect(service).toBeTruthy();
  });
});
