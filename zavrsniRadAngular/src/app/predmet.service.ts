import { Injectable } from '@angular/core';
import { Predmet } from './predmet'
import { Observable, of } from 'rxjs'
import { PomPorukeService } from './pom-poruke.service'
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { catchError, map, tap } from 'rxjs/operators';
import { RazinaStudij } from './razinaStudij';

@Injectable({
  providedIn: 'root'
})
export class PredmetService {
  private predmetURL='http://localhost:8080/predmet';
  httpOptions = {
    headers: new HttpHeaders({ 'Content-Type': 'application/json' })
  };
  private idKorisnik=1;

  constructor(private http: HttpClient, private pomPorukeService: PomPorukeService) { }

  getPredmeti(): Observable<Predmet[]>{
    return this.http.get<Predmet[]>(this.predmetURL+"/dopusteni/"+this.idKorisnik)
      .pipe(
        //tap( _ => this.log('Prikaz predmeta')),
        catchError(this.handleError<Predmet[]>('getPredmeti', []))
      );
  }

  getPredmet(id: number): Observable<Predmet>{
    return this.http.get<Predmet>(this.predmetURL+"/"+id)
      .pipe(
        //tap( _ => this.log(`Prikaz predmet ${id}`)),
        catchError(this.handleError<Predmet>(`getPredmet id=${id}`))
      );
  }

  updatePredmet(predmet: Predmet): Observable<any>{
    return this.http.put(this.predmetURL, predmet, this.httpOptions)
      .pipe(
        tap( _ => this.log(`Predmet ${predmet.nazivPredmet} je ažuriran`)),
        catchError(this.handleError<any>('updatePredmet'))
      );
  }

  addPredmet(predmet: Predmet): Observable<Predmet>{
    return this.http.post<Predmet>(this.predmetURL+"/"+this.idKorisnik, predmet, this.httpOptions)
      .pipe(
        tap((newPredmet: Predmet) => this.log(`Dodan predmet ${newPredmet.nazivPredmet}`)),
        catchError(this.handleError<Predmet>(`Dodaj predmet`))
      )
  }

  deletePredmet(predmet: Predmet): Observable<Predmet>{
    return this.http.delete<Predmet>(this.predmetURL+"/"+predmet.idPredmet, this.httpOptions)
      .pipe(
        tap( _ => this.log(`Izbrisan predmet ${predmet.nazivPredmet}`)),
        catchError(this.handleError<Predmet>('deletePredmet'))
      );
  }

  getRazine(): Observable<RazinaStudij[]>{
    return this.http.get<RazinaStudij[]>(this.predmetURL+"/razineStudij")
      .pipe(
        //tap( _ => this.log(`dohvacene razine studija`)),
        catchError(this.handleError<RazinaStudij[]>(`dohvacanje razinaStudij`))
      )
  }

  /* GET Predmet whose name contains search term */
searchPredmeti(term: string): Observable<Predmet[]> {
  if (!term.trim()) {
    // if not search term, return empty hero array.
    return of([]);
  }
  return this.http.get<Predmet[]>(this.predmetURL+"/naziv/"+term+"/"+this.idKorisnik).pipe(
    //tap( _ => this.log(`found predmet matching "${term}"`)),
    catchError(this.handleError<Predmet[]>('searchPredmet', []))
  );
}

filterPredmeti(term: string, predmeti: Predmet[]): Observable<Predmet[]>{
  if(term==""){
    return of(predmeti);
  }
  let rez: Array<Predmet> = [];
  for(let predmet of predmeti){
    if(predmet.nazivPredmet.includes(term)){
      rez.push(predmet);
    }
  }
  return of(rez);
}

filter(term: string, predmeti: Predmet[]): Predmet[]{
  if(term==""){
    return predmeti;
  }
  let rez: Array<Predmet> = [];
  for(let predmet of predmeti){
    if(predmet.nazivPredmet.search(new RegExp(term,"i")) != -1){
      rez.push(predmet);
    }
  }
  return rez;
}

  private log(poruka: string){
    this.pomPorukeService.add(`${poruka}`);
  }

      /**
     * Handle Http operation that failed.
     * Let the app continue.
     * @param operation - name of the operation that failed
     * @param result - optional value to return as the observable result
     */
    private handleError<T> (operation = 'operation', result?: T) {
      return (error: any): Observable<T> => {
     
        // TODO: send the error to remote logging infrastructure
        console.error(error); // log to console instead
     
        // TODO: better job of transforming error for user consumption
        this.log(`Operacija neuspjela!`);
     
        // Let the app keep running by returning an empty result.
        return of(result as T);
      };
    }
}
